from django.urls import path
from . import views

urlpatterns = [
    path('login/', views.loginPage, name='login'),
    path('logout/', views.logoutUser, name='logout'),
    path('register/user/', views.registerUser, name='register'),
    
    path('', views.home, name='home'),
    path('room/<str:pk>/', views.room, name='room'),
    
    path('profile/<str:pk>/', views.userProfile, name='user_profile'),
    
    path('room/create', views.createView, name='create_room'),
    path('room/<str:pk>/edit', views.updateRoomView, name='update_room'),
    path('room/<str:pk>/delete', views.deleteRoomView, name='delete_room'),
    path('room/<str:pk>/delete/message', views.deleteMessageView, name='delete_message'),
    
]
